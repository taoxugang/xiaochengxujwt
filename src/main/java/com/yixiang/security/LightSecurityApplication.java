package com.yixiang.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author hupeng <610796224@qq.com>
 */
@SpringBootApplication
public class LightSecurityApplication {
    public static void main(String[] args) {
        SpringApplication.run(LightSecurityApplication.class, args);
    }
}
