package com.yixiang.security.common.persistence.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yixiang.security.common.persistence.model.StoreMember;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface StoreMemberDao extends BaseMapper<StoreMember> {
	
}
